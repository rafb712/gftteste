﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GFTteste.Models;
using System.Threading;
using System.Globalization;

namespace GFTteste.Controllers
{
    public class CategoriaController : Controller
    {
        public IActionResult Index()
        {
            categoria rules = new categoria();
            //
            List<Trade> traders = new List<Trade>();
            Trade trader1 = new Trade();
            //
            trader1.Value = 2000000;
            trader1.ClientSector = "Private";
            traders.Add(trader1);
            //
            Trade trader2 = new Trade();
            trader2.Value = 4000000;
            trader2.ClientSector = "Public";
            traders.Add(trader2);
            //
            Trade trader3 = new Trade();
            trader3.Value = 5000000;
            trader3.ClientSector = "Public";
            traders.Add(trader3);
            //
            Trade trader4 = new Trade();
            trader4.Value = 3000000;
            trader4.ClientSector = "Public";
            traders.Add(trader4);
            //
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            List<Trade> portfolio;
            portfolio = rules.AssinaCategoria(traders);
            return View(portfolio);
        }
    }
}
