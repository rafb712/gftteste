﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GFTteste.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GFTteste.Controllers
{
    public class RulesController : Controller
    {
        // GET: RulesController
        public ActionResult Index()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Rules rules = new Rules();
            rules = rules.readRules();
            ViewData["title"] = "Categorized Bussines Rules";
            return View(rules);
        }

        // GET: RulesController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: RulesController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RulesController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                Rule r = new Rule();
                Rules newRule = new Rules();
                r.Name = collection["Name"];
                r.Value = Int32.Parse(collection["Value"]);
                r.Sector = collection["Sector"];
                r.Condition = collection["Condition"];

                if (newRule.add(r))
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return BadRequest();
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: RulesController/Edit/5
        public ActionResult Edit(string id)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Rule myRule = new Rule();
            myRule = myRule.getRule(id, false);
            return View(myRule);
        }

        // POST: RulesController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, IFormCollection collection)
        {
            try
            {
                Rule r = new Rule();
                Rules myRules = new Rules();
                r.Name = collection["Name"];
                r.Value = Int32.Parse(collection["Value"]);
                r.Sector = collection["Sector"];
                r.Condition = collection["Condition"];
                if (myRules.edit(r))
                {
                    return RedirectToAction(nameof(Index));
                } else
                {
                    return BadRequest();
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: RulesController/Delete/5
        public ActionResult Delete(string id)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Rule myRule = new Rule();
            myRule = myRule.getRule(id);
            return View(myRule);
        }

        // POST: RulesController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id, IFormCollection collection)
        {
            try
            {
                Rules myRules = new Rules();
                if (myRules.delete(id))
                {
                    return RedirectToAction(nameof(Index));
                } else
                {
                    return BadRequest();
                }
            }
            catch
            {
                return View();
            }
        }
    }
}
