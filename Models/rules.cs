﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace GFTteste.Models
{
	[XmlRoot(ElementName = "rule")]
	public class Rule
	{
		[XmlElement(ElementName = "name")]
		public string Name { get; set; }
		[XmlElement(ElementName = "value")]
		[DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = false)]
		public int Value { get; set; }
		[XmlElement(ElementName = "sector")]
		public string Sector { get; set; }
		[XmlElement(ElementName = "condition")]
		public string Condition { get; set; }

		public Rule() { }

		public Rule getRule(string id, bool del = true)
        {
			Rules lRules = new Rules();
			lRules = lRules.readRules();
            foreach (Rule r in lRules.Items.Rule)
            {
                if (r.Name == id)
                {
					if (r.Condition == "mei" && del)
					{
						r.Condition = "less than";
					}
					if (r.Condition == "may" && del)
					{
						r.Condition = "greater than";
					}
					return r;
                }
            }
			return null;
        }
	}

	[XmlRoot(ElementName = "items")]
	public class Items
	{
		[XmlElement(ElementName = "rule")]
		public List<Rule> Rule { get; set; }

		public Items() { }
	}

	[XmlRoot(ElementName = "rules")]
	public class Rules
	{
		[XmlElement(ElementName = "items")]
		public Items Items { get; set; }

		public string getCategorie(Trade t)
		{
			string tCategorie = string.Empty;
			foreach (Rule r in this.Items.Rule)
			{
				if (r.Condition == "mei")
				{
					if (t.Value <= r.Value)
					{
						tCategorie = r.Name;
					}
				}
				if (r.Condition == "may")
				{
					if (t.Value > r.Value)
					{
						if (t.ClientSector == r.Sector)
						{
							tCategorie = r.Name;
						}
					}
				}
			}
			return tCategorie;
		}
		public Rules readRules()
		{
			Serializer ser = new Serializer();
			string path = string.Empty;
			string xmlInputData = string.Empty;
			string xmlOutputData = string.Empty;

			path = Directory.GetCurrentDirectory() + @"\rules.xml";
			xmlInputData = File.ReadAllText(path);

			Rules rules = ser.Deserialize<Rules>(xmlInputData);
			xmlOutputData = ser.Serialize<Rules>(rules);

			return rules;
		}
		public bool writeRules()
		{
			try
			{
				System.Xml.Serialization.XmlSerializer writer =
				new System.Xml.Serialization.XmlSerializer(typeof(Rules));

				var path = Directory.GetCurrentDirectory() + @"\rules.xml";
				System.IO.FileStream file = System.IO.File.Create(path);

				writer.Serialize(file, this);
				file.Close();
				return true;
			} catch
			{
				return false;
			}
		}
		public Rules() { }
		public bool add(Rule r)
		{
			try
			{
				Rules rules = readRules();
				rules.Items.Rule.Add(new Rule { Name = r.Name, Value = r.Value, Sector = r.Sector, Condition = r.Condition });
				
				return rules.writeRules();
			}
			catch
			{
				return false;
			}
		}
		public bool edit(Rule r)
        {
			try
			{
                if (delete(r.Name))
                {
                    if (add(r))
                    {
						return true;
                    } else
                    {
						return false;
                    }

                } else
                {
					return false;
                }
			}
			catch
			{
				return false;
			}
		}
		public bool delete(string id)
        {
			try
			{
				Rules rules = readRules();
				rules.Items.Rule.RemoveAll(x => x.Name == id);
				return rules.writeRules();
			}
			catch
			{
				return false;
			}
		}
	}
}
