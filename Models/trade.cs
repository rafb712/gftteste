﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GFTteste.Models
{
    public class Trade : Itrade
    {
        public double Value { get; set; }
        public string ClientSector { get; set; }
        public string Categorie { get; set; }
    }
}
