﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GFTteste.Models
{
    public class categoria : Icategoria 
    {
    
        public List<Trade> AssinaCategoria(List<Trade> traders)
        {
            Rules rules = new Rules();
            rules = rules.readRules();

            List<Trade> t = new List<Trade>();
            foreach (Trade trader in traders)
            {
                t.Add(new Trade { Value = trader.Value, ClientSector = trader.ClientSector, Categorie = rules.getCategorie(trader)});
            }
            return t;
        }
    }
}
