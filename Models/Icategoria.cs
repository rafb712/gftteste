﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GFTteste.Models
{
    interface Icategoria
    {
        List<Trade> AssinaCategoria(List<Trade> traders);
    }
}
