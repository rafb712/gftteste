﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GFTteste.Models
{
    interface Itrade
    {
        double Value { get; set; }
        string ClientSector { get; set; }
        string Categorie { get; set; }
    }
}
